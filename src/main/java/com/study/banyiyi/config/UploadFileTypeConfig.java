package com.study.banyiyi.config;

import com.study.banyiyi.util.FilesUtil;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

import javax.annotation.PostConstruct;

/**
 * @ClassName UploadFileTypeConfig
 * @Description 项目初始化的时候讲文件类型进行赋值
 * @Author yangwm
 * @Date 2022/5/20 15:28
 * @Version 1.0
 */
@Configuration
@PropertySource("classpath:app.properties")
public class UploadFileTypeConfig {

    @Value("${FILETYPE}")
    private String fileType;

    @PostConstruct
    public void init(){
        System.out.println("sprigboot启动时配置"+fileType);
        FilesUtil.FILETYPE = fileType;
    }

    @Bean
    public FilesUtil filesUtil(){
        return new FilesUtil();
    }

}
