package com.study.banyiyi.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Builder;
import lombok.Data;

import java.io.Serializable;

/**
 * (TblSysFileRelation)实体类
 *
 * @author yangwm
 * @since 2021-06-28 17:38:34
 */
@Data
@Builder
@TableName("tbl_sys_file_relation")
public class SysFileRelation implements Serializable {
    private static final long serialVersionUID = 820124433399417211L;

    @TableId
    @TableField("FILE_ID")
    private String fileId;

    @TableField("FILE_NAME")
    private String fileName;

    @TableField("FILE_TYPE")
    private String fileType;

    @TableField("FILE_SIZE")
    private Double fileSize;

    @TableField("FILE_URL")
    private String fileUrl;

    @TableField("FILE_PATH")
    private String filePath;

    @TableField("CREATE_TIME")
    private String createTime;

    @TableField("CREATE_USER")
    private String createUser;

    @TableField("STATE")
    private String state;

    @TableField("EXTEND_FEILD")
    private String extendFeild;

}