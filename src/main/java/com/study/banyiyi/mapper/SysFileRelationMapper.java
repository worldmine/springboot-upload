package com.study.banyiyi.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.study.banyiyi.entity.SysFileRelation;
import org.apache.ibatis.annotations.Mapper;

/**
 * @ClassName SysFileRelationMapper
 * @Description
 * @Author yangwm
 * @Date 2021/6/28 10:49
 * @Version 1.0
 */
@Mapper
public interface SysFileRelationMapper extends BaseMapper<SysFileRelation> {

}
