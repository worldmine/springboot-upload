package com.study.banyiyi.util;

import lombok.Data;

/**
 * @ClassName ResultMsg
 * @Description 信息返回体封装
 * @Author yangwm
 * @Date 2021/6/28 17:48
 * @Version 1.0
 */
@Data
public class ResultMsg {

    private Object data;
    private String code;
    private String message;

    public ResultMsg(String message){
        this.message = message;
    }

    public ResultMsg(String code,String message){
        this.code = code;
        this.message = message;
    }

    public ResultMsg(String code,String message,Object data){
        this.code = code;
        this.message = message;
        this.data = data;
    }

}
