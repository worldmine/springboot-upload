package com.study.banyiyi.util;

import java.io.*;

/**
 * @ClassName PipedFileUtil
 * @Description 管道流相关应用工具类
 * @Author yangwm
 * @Date 2022/5/24 16:45
 * @Version 1.0
 */
public class PipedFileUtil {

    /*常见文件类型，使用字符流进行处理*/
    public static String FILETYPE;


    /**
     * pipedInputStream/pipedOutputStream
     * pipedReader/pipedWriter
     * 管道流最大用途就是在文件上传到第三方时不需要在服务中存储文件，直接根据输入流读取到输出流，提高效率
     */

    /**
     * @Description: 管道流文件处理方式 [不同线程分别执行]
     * @Param:
     * @return:
     * @Author: Yangwm
     * @Date: 2022/4/28
     */
    public void pipedOutputStreamTest() {
        try {
            PipedInputStream pis = new PipedInputStream();
            PipedOutputStream pos = new PipedOutputStream(pis);

            //先输出再读入
            WirteSteam wirteSteam = new WirteSteam(pos);
            ReadStream readStream = new ReadStream(pis);

            //执行线程
            wirteSteam.start();
            readStream.start();

        } catch (IOException e) {
            e.printStackTrace();
        } finally {

        }
    }

    //定义两个内部类，分别实现两个不同线程进行流处理
    class ReadStream extends Thread {
        //定义参数
        private PipedInputStream read;

        public ReadStream(PipedInputStream pis) {
            this.read = pis;
        }

        @Override
        public void run() {
            //将字节输出
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            byte[] buff = new byte[1024];
            int len = 0;
            try {
                //管道流的读取方法与普通流不同，只有输出流正确close时，输出流才能读到-1值
                len = read.read(buff, 0, 1024);
                bos.write(buff, 0, len);
                String s = bos.toString("utf-8");
                System.out.println("ReadSteam:" + s);
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                try {
                    bos.close();
                    read.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    class WirteSteam extends Thread {
        //定义参数
        private PipedOutputStream wirte;

        public WirteSteam(PipedOutputStream pos) {
            this.wirte = pos;
        }

        @Override
        public void run() {
            String msg = "这是一个管道流的测试数据";
            try {
                byte[] bytes = msg.getBytes("UTF-8");
                wirte.write(bytes, 0, bytes.length);
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                try {
                    wirte.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            System.out.println("WirteSteam 写入结束");
        }
    }


}
