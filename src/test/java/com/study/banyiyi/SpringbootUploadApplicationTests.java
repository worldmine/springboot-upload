package com.study.banyiyi;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.study.banyiyi.entity.SysFileRelation;
import com.study.banyiyi.mapper.SysFileRelationMapper;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.Assert;

import java.text.SimpleDateFormat;
import java.util.*;

@SpringBootTest
@RunWith(SpringRunner.class)
class SpringbootUploadApplicationTests {

    @Autowired
    private SysFileRelationMapper mapper;

    @Test
    void getPageTest(){
        Page page = new Page(1,10);
        page.setTotal(100);
        List<Map<String,Object>> list = new ArrayList<>();
        for (int i = 0; i <10 ; i++) {
           Map<String, Object> map = new HashMap<>();
           Double number = Math.random()*10;
           map.put(String.valueOf(number),"我是随机生成数"+String.valueOf(number));
           list.add(map);
        }
        page.setRecords(list);
        //对象转json
        String s = JSON.toJSONString(page);
        //json字符串转对象
        Page p = JSON.parseObject(s, Page.class);
        System.out.println(s);
    }

    @Test
    void insertFileData(){
        SimpleDateFormat sdf = new SimpleDateFormat("YYYY-MM-dd HH:mm:ss");
        String time = sdf.format(new Date());
        SysFileRelation file = SysFileRelation.builder()
                .fileName("test file")
                .filePath("C:\\Users\\MECHREVO\\Desktop\\tasklist.txt")
                .fileType("txt")
                .fileSize((double) 100)
                .fileUrl("")
                .createTime(time)
                .createUser("bamyiyi")
                .build();
        int insert = mapper.insert(file);
        Assert.isTrue(insert > 0);
    }

}
